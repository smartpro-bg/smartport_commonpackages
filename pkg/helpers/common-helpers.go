package CommonHelpers

import (
	"strconv"
)

const (
	TimeFormatLayout = "2006-01-02 15:04:05"
	MySQLTimeFormat  = "'YYYY-MM-DD HH:mm:SS'"
)

func Int64ToString(inputNum int64) string {
	return strconv.FormatInt(inputNum, 10)
}
