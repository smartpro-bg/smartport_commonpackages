package DbLayer

import (
	"database/sql"
	"errors"
	"time"

	"github.com/google/uuid"
)

type RouteCard struct {
	Id         string         `json:"Id"`
	CardNumber int            `json:"CardNumber"`
	CreateTime sql.NullTime   `json:"CreateTime"`
	VehicleID  string         `json:"VehicleID"`
	DriverId   string         `json:"DriverId"`
	CreatorId  string         `json:"CreatorId"`
	RouteId    sql.NullString `json:"RouteId"`
	IsDeleted  int            `json:"IsDeleted"`
}
type RouteCardBody struct {
	ID              string  `json:"ID"`
	RouteCardId     string  `json:"RouteCardId"`
	OrderBodyId     string  `json:"OrderBodyId"`
	DTSId           string  `json:"DTSId"`
	Quantity        float64 `json:"Quantity"`
	PackageQuantity float64 `json:"PackageQuantity"`
	IsDeleted       int     `json:"IsDeleted"`
}

//test text
func (item *RouteCard) GetRouteCard(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.Id
	if id != "" {
		where = where + " and Id = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на Нареждане")
	}

	sql := `SELECT 
			Id, 
			CardNumber, 
			CreateTime, 
			VehicleID,  
			DriverId,   
			CreatorId,  
			RouteId
			FROM 
				RouteCard and
				IsDeleted=0
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.Id, &item.CardNumber, &item.CreateTime, &item.VehicleID,
			&item.DriverId, &item.CreatorId, &item.RouteId)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *RouteCard) UpdateRouteCard(db *sql.DB) error {
	/*if item.CardNumber == 0 {
		return errors.New("Незададен номер на носител с баркод (CardNumber)")
	}*/
	if item.VehicleID == "" {
		return errors.New("Незададено превозно средство (VehicleID)")
	}
	if item.DriverId == "" {
		return errors.New("Незададен шофьор (DriverId)")
	}
	if item.CreatorId == "" {
		return errors.New("Незададен потребител (CreatorId)")
	}
	if item.RouteId.Valid == false {
		return errors.New("Незададен маршрут (RouteId)")
	}
	//
	var res sql.Result
	var err error
	if item.Id == "" {

		item.Id = uuid.New().String()
		item.CreateTime.Time = time.Now()
		item.CreateTime.Valid = true
		sql := `insert into RouteCard(
						Id,
						CardNumber,
						CreateTime,
						VehicleID,
						DriverId,
						CreatorId,
						RouteId
						 ) values (?,coalesce((SELECT MAX(mc.CardNumber)+1 FROM RouteCard as mc),1),?,?,?,?,?)`
		res, err = db.Exec(sql, item.Id, item.CreateTime, item.VehicleID, item.DriverId, item.CreatorId,
			item.RouteId)
		if err != nil {
			return err
		}

	} else {
		sql := `update   RouteCard set
						CardNumber = ?,
						CreateTime = ?,
						VehicleID = ?,
						DriverId = ?,
						CreatorId = ?,
						RouteId = ?
					where
						Id = ? `
		res, err = db.Exec(sql, item.CardNumber, item.CreateTime, item.VehicleID, item.DriverId, item.CreatorId,
			item.RouteId, item.Id)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *RouteCard) DeleteRouteCard(db *sql.DB) error {

	if item.Id == "" {
		return errors.New("Задайте id на RouteCard")
	}

	var res sql.Result
	var err error

	sql := `update   RouteCard set
						IsDeleted = 1
					where
						IsDeleted = 0 and
						Id = ? `
	res, err = db.Exec(sql, item.Id)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *RouteCardBody) GetRouteCard(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на ред от RouteCardBody")
	}

	sql := `SELECT
						ID,
						RouteCardId,
						OrderBodyId,
						DTSId,
						Quantity,
						PackageQuantity
				FROM
						RouteCardBody
				WHERE
						IsDeleted=0
				`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.RouteCardId, &item.OrderBodyId,
			&item.DTSId, &item.Quantity, &item.PackageQuantity)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *RouteCardBody) UpdateRouteCard(db *sql.DB) error {

	if item.OrderBodyId == "" {
		return errors.New("невалидна стойност за ред от нареждане(OrderBodyId)")
	}
	if item.RouteCardId == "" {
		return errors.New("невалидна стойност за(RouteCardId)")
	}
	if item.DTSId == "" {
		return errors.New("Незададена стойност за ДВС(RouteCardId)")
	}
	/*if item.Quantity <= 0 {
		return errors.New("Невалидна стойност за количество на товара (Quantity)")
	}*/
	/*if item.PackageQuantity.Valid {
		if item.PackageQuantity.Float64 <= 0 {
			return errors.New("Невалидна стойност за количество опаковки на товара (PackageQuantity)")
		}
	}*/
	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()
		sql := `insert into RouteCardBody(
					ID,
					RouteCardId,					
					OrderBodyId,
					DTSId,
					Quantity,
					PackageQuantity
					 ) values (?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.RouteCardId, item.OrderBodyId, item.DTSId, item.Quantity, item.PackageQuantity)
		if err != nil {
			return err
		}

	} else {
		sql := `update RouteCardBody set
					RouteCardId=?,					
					OrderBodyId=?,
					DTSId=?,
					Quantity=?,
					PackageQuantity=?	
				where 
					ID=?`
		res, err = db.Exec(sql, item.RouteCardId, item.OrderBodyId, item.DTSId, item.Quantity, item.PackageQuantity, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *RouteCardBody) DeleteRouteCard(db *sql.DB) error {

	if item.ID == "" {
		return errors.New("Задайте ID на ред RouteCardBody")
	}

	var res sql.Result
	var err error

	sql := `update   RouteCardBody set 
					IsDeleted = 1
				where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.ID)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}
