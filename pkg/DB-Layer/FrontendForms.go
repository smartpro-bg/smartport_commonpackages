package DbLayer

import (
	"database/sql"
	"errors"

	"github.com/google/uuid"
)

type FrontEndForm struct {
	ID          string         `json:"ID"`
	Name        string         `json:"Name"`
	DisplayName string         `json:"DisplayName"`
	ReadRoles   sql.NullString `json:"ReadRoles"`
	WriteRoles  sql.NullString `json:"WriteRoles"`
}

func (item *FrontEndForm) Get(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)
	} else {
		return errors.New("Задайте ID на форма")
	}
	name := item.Name
	if name != "" {
		where = where + " and Name = ?"
		filters = append(filters, name)
	} else {
		return errors.New("Задайте име на форма")
	}

	sql := `SELECT 
				ID,
				Name,
				DisplayName,
				ReadRoles,
				WriteRoles
			FROM 
				FrontendForms
			where
				IsDeleted=0 
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.Name, &item.DisplayName, &item.ReadRoles, &item.WriteRoles)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *FrontEndForm) Update(db *sql.DB) error {

	if item.Name == "" {
		return errors.New("Задайте вътрешно име на форма")
	}

	if item.DisplayName == "" {
		return errors.New("Задайте екранно име на форма")
	}

	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()
		sql := `insert into  FrontendForms(
					ID,
					Name,
					DisplayName,
					ReadRoles,
					WriteRoles
					 ) values (?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.Name, item.DisplayName, item.ReadRoles, item.WriteRoles)
		if err != nil {
			return err
		}

	} else {
		sql := `update   FrontendForms set 
					Name = ?,
					DisplayName= ?,
					ReadRoles= ?,
					WriteRoles = ?
				where
					ID = ? `
		res, err = db.Exec(sql, item.Name, item.DisplayName, item.ReadRoles, item.WriteRoles, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *FrontEndForm) Delete(db *sql.DB) error {

	if item.ID == "" {
		return errors.New("Задайте ID на форма")
	}

	var res sql.Result
	var err error

	sql := `update 	FrontendForms set 
					IsDelete = 1
					where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.ID)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}
