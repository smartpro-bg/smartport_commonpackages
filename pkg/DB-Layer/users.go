package DbLayer

import (
	"database/sql"
	"errors"
	"strings"
	"time"

	u "bitbucket.org/smartpro-bg/smartport_commonpackages/pkg/helpers"

	"github.com/google/uuid"
)

//
type UserContragent struct {
	UserID         string       `json:"UserID"`
	ContrID        string       `json:"ContrID"`
	StartValidDate string       `json:"StartValidDate"`
	EndValidDate   sql.NullTime `json:"EndValidDate"`
}

func (row *UserContragent) UpdateContragentToUser(db *sql.DB) error {

	//contr_id:= c.Query("id") //Param("user_id")

	if row.UserID == "" {
		return errors.New("Параметърът UserID задължителен.")
	}
	if row.StartValidDate == "" {
		return errors.New("Параметърът 'StartValidDate' задължителен.")
	}
	_, err := time.Parse(u.TimeFormatLayout, row.StartValidDate)

	if err != nil {
		return errors.New("Форматът на параметърът 'StartValidDate' е 'YYYY-MM-DD HH:mm:SS' грешка -" + err.Error())
	}
	if row.ContrID == "" {
		return errors.New("Параметърът 'ContrID' задължителен.")
	}

	sql := "select 	ID  from	User_Contragents c  " +
		" where  c. ContrID = ? and  userID=? "

	results, err := db.Query(sql, row.ContrID, row.UserID)
	defer results.Close()
	if err != nil {

		return err

	}
	var user_contr_id string
	if results.Next() {
		results.Scan(&user_contr_id)

	}
	if len(strings.Trim(user_contr_id, " ")) == 0 {

		sql = "insert into User_Contragents(UserID, ContrID, StartValidDate, EndValidDate,ID) values(?,?,?,?,?)" //, row.EndValidDate, )
		_, err = db.Exec(sql, row.UserID, row.ContrID, row.StartValidDate, row.EndValidDate, uuid.New().String())

	} else {
		sql = "update  User_Contragents set UserID=?, ContrID=?, StartValidDate =?, EndValidDate=? ,IsDeleted='0' where ID=?" // fmt.Sprintf(, endValidDate)
		_, err = db.Exec(sql, row.UserID, row.ContrID, row.StartValidDate, row.EndValidDate, user_contr_id)

	}

	if err != nil {
		return err
	}
	return nil
}
