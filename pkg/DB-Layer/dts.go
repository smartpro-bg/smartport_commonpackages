package DbLayer

import (
	"database/sql"
	"errors"
	"time"

	"github.com/google/uuid"
)

type DTS_Header struct {
	ID                string         `json:"ID"`
	ContractID        string         `json:"ContractID"`
	DocNumber         string         `json:"DocNumber"`
	DocDate           sql.NullTime   `json:"DocDate"`
	CustomHouseNumber string         `json:"CustomHouseNumber"`
	ContrID           string         `json:"ContrID"`
	PortID            string         `json:"PortID"`
	ConfirmUserID     sql.NullString `json:"ConfirmUserID"`
	CreateTime        sql.NullTime   `json:"CreateTime"`
	LastModifyTime    sql.NullTime   `json:"LastModifyTime"`
	CreatorID         string         `json:"CreatorID"`
	ModifierID        sql.NullString `json:"ModifierID"`
}
type DTS_Body struct {
	ID                  string          `json:"ID"`
	DTS_ID              string          `json:"DTS_ID"`
	UCR                 sql.NullString  `json:"UCR"`
	PackageID           sql.NullString  `json:"PackageID"`
	PackageQuantity     sql.NullFloat64 `json:"PackageQuantity"`
	Quantity            float64         `json:"Quantity"`
	GoodID              string          `json:"GoodID"`
	AttachedDocs        sql.NullString  `json:"AttachedDocs"`
	PrevDocs            sql.NullString  `json:"PrevDocs"`
	GoodExplainID       sql.NullString  `json:"GoodExplainID"`
	MeasureID           string          `json:"MeasureID"`
	ConsigneeID         sql.NullString  `json:"ConsigneeID"`
	ContainersExplainID sql.NullString  `json:"ContainersExplainID"`
}

func (item *DTS_Header) Get(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на Декларацията")
	}

	sql := `SELECT 
				ID,
				ContractID,
				DocNumber,
				DocDate,
				CustomHouseNumber,
				ContrID,
				PortID,
				ConfirmUserID,
				CreateTime,
				LastModifyTime,
				CreatorID,
				ModifierID
			FROM 
				DTS_Header 
			where
				IsDeleted=0 
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.ContractID, &item.DocNumber, &item.DocDate,
			&item.CustomHouseNumber, &item.ContrID, &item.PortID, &item.ConfirmUserID,
			&item.CreateTime, &item.LastModifyTime, &item.CreatorID, &item.ModifierID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *DTS_Header) Update(db *sql.DB) error {

	if item.ContractID == "" {
		return errors.New("Незададен контракт, по който е декларацията(ContractID)")
	}
	if item.DocNumber == "" {
		return errors.New("Незададен номер на ДВС(DocNumber)")
	}
	if item.CustomHouseNumber == "" {
		return errors.New("Незададен код на митническото учреждение (CustomHouseNumber)")
	}
	if item.PortID == "" {
		return errors.New("Незададено пристанище на складиране (PortID)")
	}
	if item.ContrID == "" {
		return errors.New("Незададен контрагент (ContrID)")
	}
	if item.ID == "" {
		if item.CreatorID == "" {
			return errors.New("Незададен потребител (CreatorID)")
		}
	} else {

		if item.ModifierID.Valid == false {
			return errors.New("Незададен потребител (ModifierID)")
		}

	}
	if item.DocDate.Valid == false {
		return errors.New("Незададена дата на документа (DocDate)")
	}
	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()
		item.CreateTime.Time = time.Now()
		item.CreateTime.Valid = true
		sql := `insert into  DTS_Header(
					ID,
					ContractID,
					DocNumber,
					DocDate,
					CustomHouseNumber,
					ContrID,
					PortID,
					ConfirmUserID,
					CreateTime,
					CreatorID
					 ) values (?,?,?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.ContractID, item.DocNumber, item.DocDate, item.CustomHouseNumber, item.ContrID,
			item.PortID, item.ConfirmUserID, item.CreateTime, item.CreatorID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   DTS_Header set 
					ContractID = ?,
					DocNumber = ?,
					DocDate= ?,
					CustomHouseNumber= ?,
					ContrID = ?,
					PortID = ?,
					ConfirmUserID= ?,
					ModifierID =?
				where
					ID = ? `
		res, err = db.Exec(sql, item.ContractID, item.DocNumber, item.DocDate, item.CustomHouseNumber, item.ContrID,
			item.PortID, item.ConfirmUserID, item.ModifierID, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *DTS_Header) Delete(db *sql.DB) error {

	if item.ID == "" {
		return errors.New("Задайте ID на Декларацията")
	}

	if item.ModifierID.Valid == false {
		return errors.New("Незададен потребител (ModifierID)")
	}

	var res sql.Result
	var err error

	sql := `update   DTS_Header set 
					IsDeleted = 1,
					ModifierID =?
				where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.ModifierID, item.ID)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

// DTS_Body

func (item *DTS_Body) Get(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на ред от Декларацията")
	}

	sql := `SELECT 
				ID,
				DTS_ID,
				UCR,
				PackageID,
				PackageQuantity,
				Quantity,
				GoodID,
				AttachedDocs,
				PrevDocs,
				GoodExplainID,
				MeasureID,
				ConsigneeID,
				ContainersExplainID
			FROM 
				DTS_Body
			WHERE
				IsDeleted=0 
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.DTS_ID, &item.UCR, &item.PackageID,
			&item.PackageQuantity, &item.Quantity, &item.GoodID, &item.AttachedDocs,
			&item.PrevDocs, &item.GoodExplainID, &item.MeasureID, &item.ConsigneeID, &item.ContainersExplainID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *DTS_Body) Update(db *sql.DB) error {

	if item.DTS_ID == "" {
		return errors.New("невалидна стойност за(DTS_ID)")
	}
	if item.GoodID == "" {
		return errors.New("Незададен товар на ДВС(GoodID)")
	}
	if item.Quantity <= 0 {
		return errors.New("Невалидна стойност за количество на товара (Quantity)")
	}
	if item.PackageQuantity.Valid {
		if item.PackageQuantity.Float64 <= 0 {
			return errors.New("Невалидна стойност за количество опаковки на товара (PackageQuantity)")
		}
	}
	if item.MeasureID == "" {
		return errors.New("невалидна стойност за(MeasureID)")
	}
	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()
		sql := `insert into  DTS_Body(
					ID,
					DTS_ID,
					UCR,
					PackageID,
					PackageQuantity,
					Quantity,
					GoodID,
					AttachedDocs,
					PrevDocs,
					GoodExplainID,
					MeasureID,
					ConsigneeID,
					ContainersExplainID
					 ) values (?,?,?,?,?,?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.DTS_ID, item.UCR, item.PackageID, item.PackageQuantity, item.Quantity, item.GoodID,
			item.AttachedDocs, item.PrevDocs, item.GoodExplainID, item.MeasureID, item.ConsigneeID, item.ContainersExplainID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   DTS_Body set
					DTS_ID=?,
					UCR=?,
					PackageID=?,
					PackageQuantity=?,
					Quantity=?,
					GoodID=?,
					AttachedDocs=?,
					PrevDocs=? ,
					GoodExplainID=?,
					MeasureID=?,
					ConsigneeID=?,
					ContainersExplainID=?
				where 
					ID=?`
		res, err = db.Exec(sql, item.DTS_ID, item.UCR, item.PackageID, item.PackageQuantity, item.Quantity, item.GoodID,
			item.AttachedDocs, item.PrevDocs, item.GoodExplainID, item.MeasureID, item.ConsigneeID, item.ContainersExplainID, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *DTS_Body) Delete(db *sql.DB) error {

	if item.ID == "" {
		return errors.New("Задайте ID на ред Декларацията")
	}

	var res sql.Result
	var err error

	sql := `update   DTS_Body set 
					IsDeleted = 1
				where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.ID)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}
