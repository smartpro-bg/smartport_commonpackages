package DbLayer

import (
	"database/sql"
	"errors"
	"time"

	"github.com/google/uuid"
)

type Orders struct {
	Id                      string         `json:"Id"`
	TypeId                  string         `json:"TypeId"`
	OrderNumber             string         `json:"OrderNumber"`
	OrderDate               sql.NullTime   `json:"OrderDate"`
	CustomImportNumber      string         `json:"CustomImportNumber"`
	CustomImportDate        sql.NullTime   `json:"CustomImportDate"`
	ApprovePortNumber       string         `json:"ApprovePortNumber"`
	ApprovedPortDateTime    sql.NullTime   `json:"ApprovedPortDateTime"`
	ContragentID            string         `json:"ContragentID"`
	PortId                  string         `json:"PortId"`
	DTSId                   string         `json:"DTSId"`
	ShipId                  string         `json:"ShipId"`
	CourseNumber            string         `json:"CourseNumber"`
	ContactPersonID         string         `json:"ContactPersonID"`
	Note                    sql.NullString `json:"Note"`
	CeoContrID              string         `json:"CeoContrID"`
	ResponsiblePortPersonId string         `json:"ResponsiblePortPersonId"`
	EXPExplainID            string         `json:"EXPExplainID"`
	RouteExplainID          string         `json:"RouteExplainID"`
	CreateTime              sql.NullTime   `json:"CreateTime"`
	LastModifyTime          sql.NullTime   `json:"LastModifyTime"`
	CreatorID               string         `json:"CreatorID"`
	ModifierID              sql.NullString `json:"ModifierID"`
}
type OrderBody struct {
	Id              string  `json:"Id"`
	OrderID         string  `json:"OrderID"`
	PackageID       string  `json:"PackageID"`
	PackageQuantity float64 `json:"PackageQuantity"`
	Quantity        float64 `json:"Quantity"`
	GoodID          string  `json:"GoodID"`
	GoodExplainId   string  `json:"GoodExplainId"`
	MeasureId       string  `json:"MeasureId"`
	IsDeleted       int     `json:"IsDeleted"`
}

type DTS_Transport_Header struct {
	ID            string       `json:"ID"`
	DTS_BodyID    string       `json:"DTS_BodyID"`
	DocDate       sql.NullTime `json:"DocDate"`
	DocNumber     string       `json:"DocNumber"`
	ExplainID     string       `json:"ExplainID"`
	ContragentID  string       `json:"ContragentID"`
	Quantity      float64      `json:"Quantity"`
	PersonID      string       `json:"PersonID"`
	CEO_PersonsID string       `json:"CEO_PersonsID"`
	IsDeleted     int          `json:"IsDeleted"`
}

type DTS_Vehicle_Drivers_List struct {
	ID                     string `json:"ID"`
	PersionID              string `json:"PersionID"`
	DTS_Transport_HeaderID string `json:"DTS_Transport_HeaderID"`
}

type DTS_Vehicles_List struct {
	ID                     string `json:"ID"`
	VehicleID              string `json:"VehicleID"`
	DTS_Transport_HeaderID string `json:"DTS_Transport_HeaderID"`
}

func (item *DTS_Vehicle_Drivers_List) GetDTSVehicleDriversList(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на DTS_Vehicle_Drivers_List")
	}

	sql := `SELECT 
			ID,
			PersionID,
			DTS_Transport_HeaderID
			FROM 
			DTS_Vehicle_Drivers_List
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.PersionID, &item.DTS_Transport_HeaderID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *DTS_Vehicle_Drivers_List) UpdateDTSVehicleDriversList(db *sql.DB) error {

	if item.PersionID == "" {
		return errors.New("Незададен PersionID (PersionID)")
	}
	if item.DTS_Transport_HeaderID == "" {
		return errors.New("Незададен  (DTS_Transport_HeaderID)")
	}

	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()

		sql := `insert into DTS_Vehicle_Drivers_List(
					ID,
					PersionID,
					DTS_Transport_HeaderID
					) values (?,?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.PersionID, item.DTS_Transport_HeaderID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   DTS_Vehicle_Drivers_List set 
				PersionID = ?,
				DTS_Transport_HeaderID = ?
				where
				ID = ? `
		res, err = db.Exec(sql, item.PersionID, item.DTS_Transport_HeaderID, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *DTS_Vehicles_List) GetDTSVehiclesList(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на DTS_Vehicles_List")
	}

	sql := `SELECT 
			ID,
			VehicleID,
			DTS_Transport_HeaderID
			FROM 
			DTS_Vehicles_List
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.VehicleID, &item.DTS_Transport_HeaderID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *DTS_Vehicles_List) UpdateDTSVehiclesList(db *sql.DB) error {

	if item.VehicleID == "" {
		return errors.New("Незададен VehicleID (VehicleID)")
	}
	if item.DTS_Transport_HeaderID == "" {
		return errors.New("Незададен DTS_Transport_HeaderID (DTS_Transport_HeaderID)")
	}

	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()

		sql := `insert into DTS_Vehicles_List(
				ID,
				VehicleID,
				DTS_Transport_HeaderID
				) values (?,?,?)`
		res, err = db.Exec(sql, item.ID, item.VehicleID, item.DTS_Transport_HeaderID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   DTS_Vehicles_List set 
				VehicleID = ?,
				DTS_Transport_HeaderID = ?
				where
				ID = ? `
		res, err = db.Exec(sql, item.VehicleID, item.DTS_Transport_HeaderID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *DTS_Transport_Header) GetDTSTransportHeader(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.ID
	if id != "" {
		where = where + " and ID = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на DTS_Transport_Header")
	}

	sql := `SELECT 
			ID,
			DTS_BodyID,
			DocDate,
			DocNumber,
			ExplainID,
			ContragentID,
			Quantity,
			PersonID,
			CEO_PersonsID	
			FROM 
			DTS_Transport_Header where
				IsDeleted=0
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.ID, &item.DTS_BodyID, &item.DocDate, &item.DocNumber,
			&item.ExplainID, &item.ContragentID, &item.Quantity, &item.PersonID,
			&item.CEO_PersonsID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *DTS_Transport_Header) UpdateDTSTransportHeader(db *sql.DB) error {

	if item.DTS_BodyID == "" {
		return errors.New("Незададен DTS_BodyID (DTS_BodyID)")
	}
	if item.DocNumber == "" {
		return errors.New("Незададен номер на документ (DocNumber)")
	}
	if item.ExplainID == "" {
		return errors.New("Незададен ExplainID (ExplainID)")
	}
	if item.ContragentID == "" {
		return errors.New("Незададен ContragentID (ContragentID)")
	}
	if item.PersonID == "" {
		return errors.New("Незададено 'PersonID' (PersonID)")
	}
	if item.CEO_PersonsID == "" {
		return errors.New("Незададена CEO_PersonsID (CEO_PersonsID)")
	}

	var res sql.Result
	var err error
	if item.ID == "" {

		item.ID = uuid.New().String()
		//item.CreateTime.Time = time.Now()
		//item.CreateTime.Valid = true
		sql := `insert into DTS_Transport_Header(
				ID,
				DTS_BodyID,
				DocDate,
				DocNumber,
				ExplainID,
				ContragentID,
				Quantity,
				PersonID,
				CEO_PersonsID
				) values (?,?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.ID, item.DTS_BodyID, item.DocDate, item.DocNumber, item.ExplainID, item.ContragentID,
			item.Quantity, item.PersonID, item.CEO_PersonsID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   DTS_Transport_Header set 
				DTS_BodyID = ?,
				DocDate = ?,             
				DocNumber = ?,
				ExplainID = ?,
				ContragentID = ?,
				Quantity = ?,
				PersonID = ?,
				CEO_PersonsID = ?
				where
				ID = ? `
		res, err = db.Exec(sql, item.DTS_BodyID, item.DocDate, item.DocNumber, item.ExplainID, item.ContragentID,
			item.Quantity, item.PersonID, item.CEO_PersonsID, item.ID)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *DTS_Transport_Header) DeleteDTSTransportHeader(db *sql.DB) error {

	if item.ID == "" {
		return errors.New("Задайте ID ")
	}

	var res sql.Result
	var err error

	sql := `update   DTS_Transport_Header set 
					IsDeleted = 1
				where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.ID)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

//test text
func (item *Orders) GetOrders(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.Id
	if id != "" {
		where = where + " and Id = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на Нареждане")
	}

	sql := `SELECT 
				Id,                      
				TypeId,
				OrderNumber,             
				OrderDate,
				CustomImportNumber,
				CustomImportDate,
				ApprovePortNumber,
				ApprovedPortDateTime,
				ContragentID,
				PortId,
				DTSId,
				ShipId,
				CourseNumber,
				ContactPersonID,
				Note,
				CeoContrID,
				ResponsiblePortPersonId,
				EXPExplainID,
				RouteExplainID,
				CreateTime,
				LastModifyTime,
				CreatorID,
				ModifierID
			FROM 
				Orders where
				IsDeleted=0
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.Id, &item.TypeId, &item.OrderNumber, &item.OrderDate,
			&item.CustomImportNumber, &item.CustomImportDate, &item.ApprovePortNumber, &item.ApprovedPortDateTime,
			&item.ContragentID, &item.PortId, &item.DTSId, &item.ShipId, &item.CourseNumber, &item.ContactPersonID,
			&item.Note, &item.CeoContrID, &item.ResponsiblePortPersonId, &item.EXPExplainID, &item.RouteExplainID,
			&item.CreateTime, &item.LastModifyTime, &item.CreatorID, &item.ModifierID)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *Orders) UpdateOrders(db *sql.DB) error {

	if item.TypeId == "" {
		return errors.New("Незададен тип на документ (TypeId)")
	}
	if item.OrderNumber == "" {
		return errors.New("Незададен номер на нареждането (OrderNumber)")
	}
	if item.CustomImportNumber == "" {
		return errors.New("Незададен митнически номер (CustomImportNumber)")
	}
	if item.ApprovePortNumber == "" {
		return errors.New("Незададен одобрен номер на пристанище (ApprovePortNumber)")
	}
	if item.PortId == "" {
		return errors.New("Незададено пристанище на складиране (PortId)")
	}
	if item.DTSId == "" {
		return errors.New("Незададена декларация за временно складиране (DTSId)")
	}
	if item.ShipId == "" {
		return errors.New("Незададен кораб (ShipId)")
	}
	if item.CourseNumber == "" {
		return errors.New("Незададен курс (CourseNumber)")
	}
	if item.ContactPersonID == "" {
		return errors.New("Незададено лице за котакт (ContactPersonID)")
	}
	if item.CeoContrID == "" {
		return errors.New("Незададен ръководител (CeoContrID)")
	}
	if item.ResponsiblePortPersonId == "" {
		return errors.New("Незададено отговорно лице (ResponsiblePortPersonId)")
	}
	if item.EXPExplainID == "" {
		return errors.New("Незададено обяснение (EXPExplainID)")
	}
	if item.RouteExplainID == "" {
		return errors.New("Незададено обяснение за маршрут (RouteExplainID)")
	}
	if item.Id == "" {
		if item.CreatorID == "" {
			return errors.New("Незададен потребител (CreatorID)")
		}
	} else {

		if item.ModifierID.Valid == false {
			return errors.New("Незададен потребител (ModifierID)")
		}

	}
	var res sql.Result
	var err error
	if item.Id == "" {

		item.Id = uuid.New().String()
		item.CreateTime.Time = time.Now()
		item.CreateTime.Valid = true
		sql := `insert into Orders(
			Id,                      
			TypeId,
			OrderNumber,             
			OrderDate,
			CustomImportNumber,
			CustomImportDate,
			ApprovePortNumber,
			ApprovedPortDateTime,
			ContragentID,
			PortId,
			DTSId,
			ShipId,
			CourseNumber,
			ContactPersonID,
			Note,
			CeoContrID,
			ResponsiblePortPersonId,
			EXPExplainID,
			RouteExplainID,
			CreateTime,			
			CreatorID,
			ModifierID 
			
					 ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.Id, item.TypeId, item.OrderNumber, item.OrderDate, item.CustomImportNumber, item.CustomImportDate,
			item.ApprovePortNumber, item.ApprovedPortDateTime, item.ContragentID, item.PortId, item.DTSId, item.ShipId, item.CourseNumber,
			item.ContactPersonID, item.Note, item.CeoContrID, item.ResponsiblePortPersonId, item.EXPExplainID,
			item.RouteExplainID, item.CreateTime, item.CreatorID, item.ModifierID)
		if err != nil {
			return err
		}

	} else {
		sql := `update   Orders set 
					TypeId = ?,
					OrderNumber = ?,             
					OrderDate = ?,
					CustomImportNumber = ?,
					CustomImportDate = ?,
					ApprovePortNumber = ?,
					ApprovedPortDateTime = ?,
					ContragentID = ?,
					PortId = ?,
					DTSId = ?,
					ShipId = ?,
					CourseNumber = ?,
					ContactPersonID = ?,
					Note = ?,
					CeoContrID = ?,
					ResponsiblePortPersonId = ?,
					EXPExplainID = ?,
					RouteExplainID = ?,
					CreateTime = ?,
					ModifierID =?
				where
					Id = ? `
		res, err = db.Exec(sql, item.TypeId, item.OrderNumber, item.OrderDate, item.CustomImportNumber, item.CustomImportDate,
			item.ApprovePortNumber, item.ApprovedPortDateTime, item.ContragentID, item.PortId, item.DTSId, item.ShipId, item.CourseNumber, item.ContactPersonID,
			item.Note, item.CeoContrID, item.ResponsiblePortPersonId, item.EXPExplainID, item.RouteExplainID, item.CreateTime, item.ModifierID, item.Id)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *Orders) DeleteOrders(db *sql.DB) error {

	if item.Id == "" {
		return errors.New("Задайте id на Неареждането")
	}

	if item.ModifierID.Valid == false {
		return errors.New("Незададен потребител (ModifierID)")
	}

	var res sql.Result
	var err error

	sql := `update   Orders set 
					IsDeleted = 1,
					ModifierID =?
				where
					IsDeleted = 0 and 
					Id = ? `
	res, err = db.Exec(sql, item.ModifierID, item.Id)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *OrderBody) GetOrders(db *sql.DB) error {
	var where string

	var filters []interface{}

	id := item.Id
	if id != "" {
		where = where + " and Id = ?"
		filters = append(filters, id)

	} else {
		return errors.New("Задайте ID на ред от нареждането")
	}

	sql := `SELECT 
				Id,
				OrderID,				
				PackageID,
				PackageQuantity,
				Quantity,
				GoodID,
				GoodExplainId,
				MeasureId				
			FROM 
				OrderBody
			WHERE
				IsDeleted=0 
			`
	if len(where) > 0 {
		sql = sql + where
	}

	results, err := db.Query(sql, filters...)
	if err != nil {
		return err
	}

	for results.Next() {
		err = results.Scan(&item.Id, &item.OrderID, &item.PackageID,
			&item.PackageQuantity, &item.Quantity, &item.GoodID, &item.GoodExplainId, &item.MeasureId)

		if err != nil {
			return err
		}
	}
	return nil
}

func (item *OrderBody) UpdateOrders(db *sql.DB) error {

	if item.OrderID == "" {
		return errors.New("невалидна стойност за(OrderID)")
	}
	if item.GoodID == "" {
		return errors.New("Незададен товар на ДВС(GoodID)")
	}
	/*if item.Quantity <= 0 {
		return errors.New("Невалидна стойност за количество на товара (Quantity)")
	}*/
	/*if item.PackageQuantity.Valid {
		if item.PackageQuantity.Float64 <= 0 {
			return errors.New("Невалидна стойност за количество опаковки на товара (PackageQuantity)")
		}
	}*/
	var res sql.Result
	var err error
	if item.Id == "" {

		item.Id = uuid.New().String()
		sql := `insert into  OrderBody(
					Id,
					OrderID,					
					PackageID,
					PackageQuantity,
					Quantity,
					GoodID,
					GoodExplainId,
					MeasureId
					 ) values (?,?,?,?,?,?,?,?)`
		res, err = db.Exec(sql, item.Id, item.OrderID, item.PackageID, item.PackageQuantity, item.Quantity, item.GoodID, item.GoodExplainId, item.MeasureId)
		if err != nil {
			return err
		}

	} else {
		sql := `update   OrderBody set
					OrderID=?,					
					PackageID=?,
					PackageQuantity=?,
					Quantity=?,
					GoodID=?,
					GoodExplainId=?,
					MeasureId=? 
				where 
					Id=?`
		res, err = db.Exec(sql, item.OrderID, item.PackageID, item.PackageQuantity, item.Quantity, item.GoodID, item.GoodExplainId, item.MeasureId,
			item.Id)
		if err != nil {
			return err
		}

	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}

func (item *OrderBody) DeleteOrders(db *sql.DB) error {

	if item.Id == "" {
		return errors.New("Задайте ID на ред нареждането")
	}

	var res sql.Result
	var err error

	sql := `update   OrderBody set 
					IsDeleted = 1
				where
					IsDeleted = 0 and 
					ID = ? `
	res, err = db.Exec(sql, item.Id)
	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}
	return nil
}
