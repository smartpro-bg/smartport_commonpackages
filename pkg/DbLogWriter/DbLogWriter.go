package DbLogWriter

import (
	"database/sql"
	"fmt"
)

type DbData struct {
	Username string
	Password string
	DbName   string
	DbHost   string
	DbPort   string
}
type LogMessage struct {
	UserID    string
	Operation string
	TableName sql.NullString
	OldData   string
	NewData   string
}

func connect(username string, password string, dbName string, dbHost string, dbPort string) (*sql.DB, error) {
	conn, err := sql.Open("mysql", username+":"+password+"@tcp("+dbHost+":"+dbPort+")/"+dbName)

	if err != nil {
		return nil, err
	}

	return conn, nil

}

func dbConfig(username string, password string, dbName string, dbHost string, dbPort string) *sql.DB {
	db, err := connect(username, password, dbName, dbHost, dbPort)
	if err != nil {
		fmt.Println("Error while connecting to logger database")
		panic(err)
	}
	fmt.Println("MySQL is Running..")
	return db
}

func AddLogMessage(logDb DbData, logMess LogMessage, mess *[][]string) bool {
	db := *dbConfig(logDb.Username, logDb.Password, logDb.DbName, logDb.DbHost, logDb.DbPort)
	defer db.Close()

	isValidParams := true

	if logMess.UserID == "" {
		*mess = append(*mess, []string{"Error", "Параметърът ':UserID' задължителен."})
		isValidParams = false
	}
	if logMess.Operation == "" {
		*mess = append(*mess, []string{"Error", "Параметърът ':Operation' е задължителен."})
		isValidParams = false
	}
	if !isValidParams {
		return false

	}

	sql := `insert into Operation_Log (
			UserID,
			Operation,
			TableName,
			OldData,
			NewData
		) values(?,?,?,?,?)`
	res, err := db.Exec(sql, logMess.UserID, logMess.Operation, logMess.TableName, logMess.OldData, logMess.NewData)

	if err != nil {
		fmt.Print(err.Error())
		*mess = append(*mess, []string{"Error", err.Error()})
		return false
	}

	count, err := res.RowsAffected()
	if err != nil {
		fmt.Print(err.Error())
		*mess = append(*mess, []string{"Error", err.Error()})
		return false

	}
	fmt.Println(count)
	return true
}
